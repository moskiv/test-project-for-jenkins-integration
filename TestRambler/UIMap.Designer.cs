﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by coded UI test builder.
//      Version: 15.0.0.0
//
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------

namespace TestRambler
{
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text.RegularExpressions;
    using System.Windows.Input;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    
    
    [GeneratedCode("Coded UITest Builder", "15.0.26208.0")]
    public partial class UIMap
    {
        
        /// <summary>
        /// RamblerRu - Use 'RamblerRuParams' to pass parameters into this method.
        /// </summary>
        public void RamblerRu()
        {
            #region Variable Declarations
            WinPane uIItemPane = this.UIInstallandconfigureTWindow.UINewTabButton.UIItemPane;
            WinPane uIItemPane1 = this.UIInstallandconfigureTWindow.UIItemGroup.UIItemPane;
            WinEdit uIAddressandsearchbarEdit = this.UIInstallandconfigureTWindow.UIItemGroup.UIAddressandsearchbarEdit;
            WinPane uIItemPane2 = this.UIInstallandconfigureTWindow.UIРамблерновостипочтаиTabPage.UIItemPane;
            #endregion

            // Click pane
            Mouse.Click(uIItemPane, new Point(20, 19));

            // Click pane
            Mouse.Click(uIItemPane1, new Point(1600, 18));

            // Type 'rambler.ru' in 'Address and search bar' text box
            uIAddressandsearchbarEdit.Text = this.RamblerRuParams.UIAddressandsearchbarEditText;

            // Type '{Enter}' in 'Address and search bar' text box
            Keyboard.SendKeys(uIAddressandsearchbarEdit, this.RamblerRuParams.UIAddressandsearchbarEditSendKeys, ModifierKeys.None);

            // Click pane numbered 4 in 'Рамблер/новости, почта и поиск — медийный портал: ...' tab
            Mouse.Click(uIItemPane2, new Point(57, 15));
        }
        
        /// <summary>
        /// FolderoPen
        /// </summary>
        public void FolderoPen()
        {
            #region Variable Declarations
            WinListItem uITestFolderListItem = this.UIProgramManagerWindow.UIDesktopList.UITestFolderListItem;
            WinButton uICloseButton = this.UITestFolderWindow.UITestFolderTitleBar.UICloseButton;
            #endregion

            // Double-Click 'testFolder' list item
            Mouse.DoubleClick(uITestFolderListItem, new Point(53, 25));

            // Click 'Close' button
            Mouse.Click(uICloseButton, new Point(20, 7));
        }
        
        /// <summary>
        /// TFS - Use 'TFSParams' to pass parameters into this method.
        /// </summary>
        public void TFS()
        {
            #region Variable Declarations
            WinPane uIItemPane = this.UIInstallandconfigureTWindow.UINewTabButton.UIItemPane;
            WinPane uIItemPane1 = this.UIInstallandconfigureTWindow.UIItemGroup.UIItemPane;
            WinEdit uIAddressandsearchbarEdit = this.UIInstallandconfigureTWindow.UIItemGroup.UIAddressandsearchbarEdit;
            WinPane uIItemPane2 = this.UIInstallandconfigureTWindow.UIРамблерновостипочтаиTabPage.UIItemPane;
            #endregion

            // Click pane
            Mouse.Click(uIItemPane, new Point(22, 22));

            // Click pane
            Mouse.Click(uIItemPane1, new Point(1552, 17));

            // Type 'rambler.ru' in 'Address and search bar' text box
            uIAddressandsearchbarEdit.Text = this.TFSParams.UIAddressandsearchbarEditText;

            // Type '{Enter}' in 'Address and search bar' text box
            Keyboard.SendKeys(uIAddressandsearchbarEdit, this.TFSParams.UIAddressandsearchbarEditSendKeys, ModifierKeys.None);

            // Click pane numbered 4 in 'Рамблер/новости, почта и поиск — медийный портал: ...' tab
            Mouse.Click(uIItemPane2, new Point(150, 20));
        }
        
        /// <summary>
        /// yup - Use 'yupParams' to pass parameters into this method.
        /// </summary>
        public void yup()
        {
            #region Variable Declarations
            WinPane uIItemPane = this.UIInstallandconfigureTWindow.UINewTabButton.UIItemPane;
            WinPane uIItemPane1 = this.UIInstallandconfigureTWindow.UIItemGroup.UIItemPane;
            WinEdit uIAddressandsearchbarEdit = this.UIInstallandconfigureTWindow.UIItemGroup.UIAddressandsearchbarEdit;
            WinPane uIItemPane2 = this.UIInstallandconfigureTWindow.UIРамблерновостипочтаиTabPage.UIItemPane;
            #endregion

            // Click pane
            Mouse.Click(uIItemPane, new Point(12, 16));

            // Click pane
            Mouse.Click(uIItemPane1, new Point(1590, 20));

            // Type 'rambler.ru' in 'Address and search bar' text box
            uIAddressandsearchbarEdit.Text = this.yupParams.UIAddressandsearchbarEditText;

            // Type '{Enter}' in 'Address and search bar' text box
            Keyboard.SendKeys(uIAddressandsearchbarEdit, this.yupParams.UIAddressandsearchbarEditSendKeys, ModifierKeys.None);

            // Click pane numbered 4 in 'Рамблер/новости, почта и поиск — медийный портал: ...' tab
            Mouse.Click(uIItemPane2, new Point(161, 16));
        }
        
        /// <summary>
        /// RecordedMethod1
        /// </summary>
        public void RecordedMethod1()
        {
            #region Variable Declarations
            WinPane uIItemPane = this.UIAnothercontrolisblocWindow.UIInstallandconfigureATabPage.UIItemPane;
            WinPane uIItemPane1 = this.UIAnothercontrolisblocWindow.UIInstallandconfigureaTabPage1.UIItemPane;
            WinPane uIItemPane2 = this.UIAnothercontrolisblocWindow.UIDownloadsIDECodeTeamTabPage.UIItemPane;
            #endregion

            // Click pane numbered 4 in 'Install and configure - Azure DevOps Server & TFS ...' tab
            Mouse.Click(uIItemPane, new Point(85, 1));

            // Click pane numbered 4 in 'Install and configure a single server - Azure DevO...' tab
            Mouse.Click(uIItemPane1, new Point(91, 7));

            // Click pane numbered 4 in 'Downloads | IDE, Code, & Team Foundation Server | ...' tab
            Mouse.Click(uIItemPane2, new Point(108, 11));

            // Click pane numbered 4 in 'Install and configure a single server - Azure DevO...' tab
            Mouse.Click(uIItemPane1, new Point(105, 11));

            // Click pane numbered 4 in 'Install and configure - Azure DevOps Server & TFS ...' tab
            Mouse.Click(uIItemPane, new Point(110, 8));
        }
        
        #region Properties
        public virtual RamblerRuParams RamblerRuParams
        {
            get
            {
                if ((this.mRamblerRuParams == null))
                {
                    this.mRamblerRuParams = new RamblerRuParams();
                }
                return this.mRamblerRuParams;
            }
        }
        
        public virtual TFSParams TFSParams
        {
            get
            {
                if ((this.mTFSParams == null))
                {
                    this.mTFSParams = new TFSParams();
                }
                return this.mTFSParams;
            }
        }
        
        public virtual yupParams yupParams
        {
            get
            {
                if ((this.myupParams == null))
                {
                    this.myupParams = new yupParams();
                }
                return this.myupParams;
            }
        }
        
        public UIInstallandconfigureTWindow UIInstallandconfigureTWindow
        {
            get
            {
                if ((this.mUIInstallandconfigureTWindow == null))
                {
                    this.mUIInstallandconfigureTWindow = new UIInstallandconfigureTWindow();
                }
                return this.mUIInstallandconfigureTWindow;
            }
        }
        
        public UIProgramManagerWindow UIProgramManagerWindow
        {
            get
            {
                if ((this.mUIProgramManagerWindow == null))
                {
                    this.mUIProgramManagerWindow = new UIProgramManagerWindow();
                }
                return this.mUIProgramManagerWindow;
            }
        }
        
        public UITestFolderWindow UITestFolderWindow
        {
            get
            {
                if ((this.mUITestFolderWindow == null))
                {
                    this.mUITestFolderWindow = new UITestFolderWindow();
                }
                return this.mUITestFolderWindow;
            }
        }
        
        public UIAnothercontrolisblocWindow UIAnothercontrolisblocWindow
        {
            get
            {
                if ((this.mUIAnothercontrolisblocWindow == null))
                {
                    this.mUIAnothercontrolisblocWindow = new UIAnothercontrolisblocWindow();
                }
                return this.mUIAnothercontrolisblocWindow;
            }
        }
        #endregion
        
        #region Fields
        private RamblerRuParams mRamblerRuParams;
        
        private TFSParams mTFSParams;
        
        private yupParams myupParams;
        
        private UIInstallandconfigureTWindow mUIInstallandconfigureTWindow;
        
        private UIProgramManagerWindow mUIProgramManagerWindow;
        
        private UITestFolderWindow mUITestFolderWindow;
        
        private UIAnothercontrolisblocWindow mUIAnothercontrolisblocWindow;
        #endregion
    }
    
    /// <summary>
    /// Parameters to be passed into 'RamblerRu'
    /// </summary>
    [GeneratedCode("Coded UITest Builder", "15.0.26208.0")]
    public class RamblerRuParams
    {
        
        #region Fields
        /// <summary>
        /// Type 'rambler.ru' in 'Address and search bar' text box
        /// </summary>
        public string UIAddressandsearchbarEditText = "rambler.ru";
        
        /// <summary>
        /// Type '{Enter}' in 'Address and search bar' text box
        /// </summary>
        public string UIAddressandsearchbarEditSendKeys = "{Enter}";
        #endregion
    }
    
    /// <summary>
    /// Parameters to be passed into 'TFS'
    /// </summary>
    [GeneratedCode("Coded UITest Builder", "15.0.26208.0")]
    public class TFSParams
    {
        
        #region Fields
        /// <summary>
        /// Type 'rambler.ru' in 'Address and search bar' text box
        /// </summary>
        public string UIAddressandsearchbarEditText = "rambler.ru";
        
        /// <summary>
        /// Type '{Enter}' in 'Address and search bar' text box
        /// </summary>
        public string UIAddressandsearchbarEditSendKeys = "{Enter}";
        #endregion
    }
    
    /// <summary>
    /// Parameters to be passed into 'yup'
    /// </summary>
    [GeneratedCode("Coded UITest Builder", "15.0.26208.0")]
    public class yupParams
    {
        
        #region Fields
        /// <summary>
        /// Type 'rambler.ru' in 'Address and search bar' text box
        /// </summary>
        public string UIAddressandsearchbarEditText = "rambler.ru";
        
        /// <summary>
        /// Type '{Enter}' in 'Address and search bar' text box
        /// </summary>
        public string UIAddressandsearchbarEditSendKeys = "{Enter}";
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "15.0.26208.0")]
    public class UIInstallandconfigureTWindow : WinWindow
    {
        
        public UIInstallandconfigureTWindow()
        {
            #region Search Criteria
            this.SearchProperties[WinWindow.PropertyNames.Name] = "Install and configure Team Foundation Server - Azure DevOps Server & TFS | Micros" +
                "oft Docs - Google Chrome";
            this.SearchProperties[WinWindow.PropertyNames.ClassName] = "Chrome_WidgetWin_1";
            this.WindowTitles.Add("Install and configure Team Foundation Server - Azure DevOps Server & TFS | Micros" +
                    "oft Docs - Google Chrome");
            this.WindowTitles.Add("New Tab - Google Chrome");
            this.WindowTitles.Add("Рамблер/новости, почта и поиск — медийный портал: новости России и мира, электрон" +
                    "ная почта, погода, развлекательные и коммуникационные сервисы. Новости сегодня и" +
                    " сейчас - Google Chrome");
            #endregion
        }
        
        #region Properties
        public UINewTabButton UINewTabButton
        {
            get
            {
                if ((this.mUINewTabButton == null))
                {
                    this.mUINewTabButton = new UINewTabButton(this);
                }
                return this.mUINewTabButton;
            }
        }
        
        public UIItemGroup UIItemGroup
        {
            get
            {
                if ((this.mUIItemGroup == null))
                {
                    this.mUIItemGroup = new UIItemGroup(this);
                }
                return this.mUIItemGroup;
            }
        }
        
        public UIРамблерновостипочтаиTabPage UIРамблерновостипочтаиTabPage
        {
            get
            {
                if ((this.mUIРамблерновостипочтаиTabPage == null))
                {
                    this.mUIРамблерновостипочтаиTabPage = new UIРамблерновостипочтаиTabPage(this);
                }
                return this.mUIРамблерновостипочтаиTabPage;
            }
        }
        #endregion
        
        #region Fields
        private UINewTabButton mUINewTabButton;
        
        private UIItemGroup mUIItemGroup;
        
        private UIРамблерновостипочтаиTabPage mUIРамблерновостипочтаиTabPage;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "15.0.26208.0")]
    public class UINewTabButton : WinButton
    {
        
        public UINewTabButton(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WinButton.PropertyNames.Name] = "New Tab";
            this.WindowTitles.Add("Install and configure Team Foundation Server - Azure DevOps Server & TFS | Micros" +
                    "oft Docs - Google Chrome");
            #endregion
        }
        
        #region Properties
        public WinPane UIItemPane
        {
            get
            {
                if ((this.mUIItemPane == null))
                {
                    this.mUIItemPane = new WinPane(this);
                    #region Search Criteria
                    this.mUIItemPane.WindowTitles.Add("Install and configure Team Foundation Server - Azure DevOps Server & TFS | Micros" +
                            "oft Docs - Google Chrome");
                    #endregion
                }
                return this.mUIItemPane;
            }
        }
        #endregion
        
        #region Fields
        private WinPane mUIItemPane;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "15.0.26208.0")]
    public class UIItemGroup : WinGroup
    {
        
        public UIItemGroup(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.WindowTitles.Add("New Tab - Google Chrome");
            #endregion
        }
        
        #region Properties
        public WinPane UIItemPane
        {
            get
            {
                if ((this.mUIItemPane == null))
                {
                    this.mUIItemPane = new WinPane(this);
                    #region Search Criteria
                    this.mUIItemPane.SearchProperties[WinControl.PropertyNames.Instance] = "2";
                    this.mUIItemPane.WindowTitles.Add("New Tab - Google Chrome");
                    #endregion
                }
                return this.mUIItemPane;
            }
        }
        
        public WinEdit UIAddressandsearchbarEdit
        {
            get
            {
                if ((this.mUIAddressandsearchbarEdit == null))
                {
                    this.mUIAddressandsearchbarEdit = new WinEdit(this);
                    #region Search Criteria
                    this.mUIAddressandsearchbarEdit.SearchProperties[WinEdit.PropertyNames.Name] = "Address and search bar";
                    this.mUIAddressandsearchbarEdit.WindowTitles.Add("New Tab - Google Chrome");
                    #endregion
                }
                return this.mUIAddressandsearchbarEdit;
            }
        }
        #endregion
        
        #region Fields
        private WinPane mUIItemPane;
        
        private WinEdit mUIAddressandsearchbarEdit;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "15.0.26208.0")]
    public class UIРамблерновостипочтаиTabPage : WinTabPage
    {
        
        public UIРамблерновостипочтаиTabPage(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WinTabPage.PropertyNames.Name] = "Рамблер/новости, почта и поиск — медийный портал: новости России и мира, электрон" +
                "ная почта, погода, развлекательные и коммуникационные сервисы. Новости сегодня и" +
                " сейчас";
            this.WindowTitles.Add("Рамблер/новости, почта и поиск — медийный портал: новости России и мира, электрон" +
                    "ная почта, погода, развлекательные и коммуникационные сервисы. Новости сегодня и" +
                    " сейчас - Google Chrome");
            #endregion
        }
        
        #region Properties
        public WinPane UIItemPane
        {
            get
            {
                if ((this.mUIItemPane == null))
                {
                    this.mUIItemPane = new WinPane(this);
                    #region Search Criteria
                    this.mUIItemPane.SearchProperties[WinControl.PropertyNames.Instance] = "4";
                    this.mUIItemPane.WindowTitles.Add("Рамблер/новости, почта и поиск — медийный портал: новости России и мира, электрон" +
                            "ная почта, погода, развлекательные и коммуникационные сервисы. Новости сегодня и" +
                            " сейчас - Google Chrome");
                    #endregion
                }
                return this.mUIItemPane;
            }
        }
        #endregion
        
        #region Fields
        private WinPane mUIItemPane;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "15.0.26208.0")]
    public class UIProgramManagerWindow : WinWindow
    {
        
        public UIProgramManagerWindow()
        {
            #region Search Criteria
            this.SearchProperties[WinWindow.PropertyNames.Name] = "Program Manager";
            this.SearchProperties[WinWindow.PropertyNames.ClassName] = "Progman";
            this.WindowTitles.Add("Program Manager");
            #endregion
        }
        
        #region Properties
        public UIDesktopList UIDesktopList
        {
            get
            {
                if ((this.mUIDesktopList == null))
                {
                    this.mUIDesktopList = new UIDesktopList(this);
                }
                return this.mUIDesktopList;
            }
        }
        #endregion
        
        #region Fields
        private UIDesktopList mUIDesktopList;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "15.0.26208.0")]
    public class UIDesktopList : WinList
    {
        
        public UIDesktopList(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WinList.PropertyNames.Name] = "Desktop";
            this.WindowTitles.Add("Program Manager");
            #endregion
        }
        
        #region Properties
        public WinListItem UITestFolderListItem
        {
            get
            {
                if ((this.mUITestFolderListItem == null))
                {
                    this.mUITestFolderListItem = new WinListItem(this);
                    #region Search Criteria
                    this.mUITestFolderListItem.SearchProperties[WinListItem.PropertyNames.Name] = "testFolder";
                    this.mUITestFolderListItem.WindowTitles.Add("Program Manager");
                    #endregion
                }
                return this.mUITestFolderListItem;
            }
        }
        #endregion
        
        #region Fields
        private WinListItem mUITestFolderListItem;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "15.0.26208.0")]
    public class UITestFolderWindow : WinWindow
    {
        
        public UITestFolderWindow()
        {
            #region Search Criteria
            this.SearchProperties[WinWindow.PropertyNames.Name] = "testFolder";
            this.SearchProperties[WinWindow.PropertyNames.ClassName] = "CabinetWClass";
            this.WindowTitles.Add("testFolder");
            #endregion
        }
        
        #region Properties
        public UITestFolderTitleBar UITestFolderTitleBar
        {
            get
            {
                if ((this.mUITestFolderTitleBar == null))
                {
                    this.mUITestFolderTitleBar = new UITestFolderTitleBar(this);
                }
                return this.mUITestFolderTitleBar;
            }
        }
        #endregion
        
        #region Fields
        private UITestFolderTitleBar mUITestFolderTitleBar;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "15.0.26208.0")]
    public class UITestFolderTitleBar : WinTitleBar
    {
        
        public UITestFolderTitleBar(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.WindowTitles.Add("testFolder");
            #endregion
        }
        
        #region Properties
        public WinButton UICloseButton
        {
            get
            {
                if ((this.mUICloseButton == null))
                {
                    this.mUICloseButton = new WinButton(this);
                    #region Search Criteria
                    this.mUICloseButton.SearchProperties[WinButton.PropertyNames.Name] = "Close";
                    this.mUICloseButton.WindowTitles.Add("testFolder");
                    #endregion
                }
                return this.mUICloseButton;
            }
        }
        #endregion
        
        #region Fields
        private WinButton mUICloseButton;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "15.0.26208.0")]
    public class UIAnothercontrolisblocWindow : WinWindow
    {
        
        public UIAnothercontrolisblocWindow()
        {
            #region Search Criteria
            this.SearchProperties[WinWindow.PropertyNames.Name] = "Another control is blocking the control. Please make the blocked control visible " +
                "and retry the action. Additional Details: - Google Chrome";
            this.SearchProperties[WinWindow.PropertyNames.ClassName] = "Chrome_WidgetWin_1";
            this.WindowTitles.Add("Another control is blocking the control. Please make the blocked control visible " +
                    "and retry the action. Additional Details: - Google Chrome");
            this.WindowTitles.Add("Install and configure - Azure DevOps Server & TFS | Microsoft Docs - Google Chrom" +
                    "e");
            this.WindowTitles.Add("Install and configure a single server - Azure DevOps Server & TFS | Microsoft Doc" +
                    "s - Google Chrome");
            this.WindowTitles.Add("Downloads | IDE, Code, & Team Foundation Server | Visual Studio - Google Chrome");
            #endregion
        }
        
        #region Properties
        public UIInstallandconfigureATabPage UIInstallandconfigureATabPage
        {
            get
            {
                if ((this.mUIInstallandconfigureATabPage == null))
                {
                    this.mUIInstallandconfigureATabPage = new UIInstallandconfigureATabPage(this);
                }
                return this.mUIInstallandconfigureATabPage;
            }
        }
        
        public UIInstallandconfigureaTabPage1 UIInstallandconfigureaTabPage1
        {
            get
            {
                if ((this.mUIInstallandconfigureaTabPage1 == null))
                {
                    this.mUIInstallandconfigureaTabPage1 = new UIInstallandconfigureaTabPage1(this);
                }
                return this.mUIInstallandconfigureaTabPage1;
            }
        }
        
        public UIDownloadsIDECodeTeamTabPage UIDownloadsIDECodeTeamTabPage
        {
            get
            {
                if ((this.mUIDownloadsIDECodeTeamTabPage == null))
                {
                    this.mUIDownloadsIDECodeTeamTabPage = new UIDownloadsIDECodeTeamTabPage(this);
                }
                return this.mUIDownloadsIDECodeTeamTabPage;
            }
        }
        #endregion
        
        #region Fields
        private UIInstallandconfigureATabPage mUIInstallandconfigureATabPage;
        
        private UIInstallandconfigureaTabPage1 mUIInstallandconfigureaTabPage1;
        
        private UIDownloadsIDECodeTeamTabPage mUIDownloadsIDECodeTeamTabPage;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "15.0.26208.0")]
    public class UIInstallandconfigureATabPage : WinTabPage
    {
        
        public UIInstallandconfigureATabPage(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WinTabPage.PropertyNames.Name] = "Install and configure - Azure DevOps Server & TFS | Microsoft Docs";
            this.WindowTitles.Add("Another control is blocking the control. Please make the blocked control visible " +
                    "and retry the action. Additional Details: - Google Chrome");
            this.WindowTitles.Add("Install and configure a single server - Azure DevOps Server & TFS | Microsoft Doc" +
                    "s - Google Chrome");
            #endregion
        }
        
        #region Properties
        public WinPane UIItemPane
        {
            get
            {
                if ((this.mUIItemPane == null))
                {
                    this.mUIItemPane = new WinPane(this);
                    #region Search Criteria
                    this.mUIItemPane.SearchProperties[WinControl.PropertyNames.Instance] = "4";
                    this.mUIItemPane.WindowTitles.Add("Another control is blocking the control. Please make the blocked control visible " +
                            "and retry the action. Additional Details: - Google Chrome");
                    this.mUIItemPane.WindowTitles.Add("Install and configure a single server - Azure DevOps Server & TFS | Microsoft Doc" +
                            "s - Google Chrome");
                    #endregion
                }
                return this.mUIItemPane;
            }
        }
        #endregion
        
        #region Fields
        private WinPane mUIItemPane;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "15.0.26208.0")]
    public class UIInstallandconfigureaTabPage1 : WinTabPage
    {
        
        public UIInstallandconfigureaTabPage1(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WinTabPage.PropertyNames.Name] = "Install and configure a single server - Azure DevOps Server & TFS | Microsoft Doc" +
                "s";
            this.WindowTitles.Add("Install and configure - Azure DevOps Server & TFS | Microsoft Docs - Google Chrom" +
                    "e");
            this.WindowTitles.Add("Downloads | IDE, Code, & Team Foundation Server | Visual Studio - Google Chrome");
            #endregion
        }
        
        #region Properties
        public WinPane UIItemPane
        {
            get
            {
                if ((this.mUIItemPane == null))
                {
                    this.mUIItemPane = new WinPane(this);
                    #region Search Criteria
                    this.mUIItemPane.SearchProperties[WinControl.PropertyNames.Instance] = "4";
                    this.mUIItemPane.WindowTitles.Add("Install and configure - Azure DevOps Server & TFS | Microsoft Docs - Google Chrom" +
                            "e");
                    this.mUIItemPane.WindowTitles.Add("Downloads | IDE, Code, & Team Foundation Server | Visual Studio - Google Chrome");
                    #endregion
                }
                return this.mUIItemPane;
            }
        }
        #endregion
        
        #region Fields
        private WinPane mUIItemPane;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "15.0.26208.0")]
    public class UIDownloadsIDECodeTeamTabPage : WinTabPage
    {
        
        public UIDownloadsIDECodeTeamTabPage(UITestControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[WinTabPage.PropertyNames.Name] = "Downloads | IDE, Code, & Team Foundation Server | Visual Studio";
            this.WindowTitles.Add("Install and configure a single server - Azure DevOps Server & TFS | Microsoft Doc" +
                    "s - Google Chrome");
            #endregion
        }
        
        #region Properties
        public WinPane UIItemPane
        {
            get
            {
                if ((this.mUIItemPane == null))
                {
                    this.mUIItemPane = new WinPane(this);
                    #region Search Criteria
                    this.mUIItemPane.SearchProperties[WinControl.PropertyNames.Instance] = "4";
                    this.mUIItemPane.WindowTitles.Add("Install and configure a single server - Azure DevOps Server & TFS | Microsoft Doc" +
                            "s - Google Chrome");
                    #endregion
                }
                return this.mUIItemPane;
            }
        }
        #endregion
        
        #region Fields
        private WinPane mUIItemPane;
        #endregion
    }
}
